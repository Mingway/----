//
//  AppDelegate.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-16.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PadRootViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UINavigationController *navController;
@property (nonatomic, strong) PadRootViewController *padRootViewController;

@end
