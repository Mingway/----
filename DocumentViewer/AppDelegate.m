//
//  AppDelegate.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-16.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "AppDelegate.h"
#import "MFSideMenuContainerViewController.h"
#import "PadMenuViewController.h"
#import "PadRootViewController.h"
#import "ModelManager.h"

@interface AppDelegate (){
    MFSideMenuContainerViewController *_sideMenuContainerViewController;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Phone" bundle:nil] instantiateInitialViewController];
    }else{
        _padRootViewController = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadRootViewController"];
        PadMenuViewController *menu = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadMenuViewController"];
        _navController = [[UINavigationController alloc] initWithRootViewController:_padRootViewController];
        _sideMenuContainerViewController = [MFSideMenuContainerViewController containerWithCenterViewController:_navController leftMenuViewController:menu rightMenuViewController:nil] ;
        self.window.rootViewController = _sideMenuContainerViewController;
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    if ([url isFileURL]) {
        NSLog(@"open file");
        [_padRootViewController performSelectorOnMainThread:@selector(handleOpenDocumentFromInbox:) withObject:url waitUntilDone:NO];
    }
    return YES;
}

#pragma mark -
#pragma mark Others

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[ModelManager defaultManager] saveContext];
}

@end
