//
//  PadDocumentCell.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-16.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PadDocumentCell : UICollectionViewCell

- (void)setTitle:(NSString *)title;
- (void)setDetail:(NSString *)detail;
- (void)setPreviewImage:(UIImage *)image;

@end
