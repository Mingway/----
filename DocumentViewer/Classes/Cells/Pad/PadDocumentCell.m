//
//  PadDocumentCell.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-16.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PadDocumentCell.h"

@interface PadDocumentCell ()
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end

@implementation PadDocumentCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setTitle:(NSString *)title{
    self.titleLabel.text = title;
}

- (void)setDetail:(NSString *)detail{
    self.detailLabel.text = detail;
}

- (void)setPreviewImage:(UIImage *)image{
    self.previewImageView.image = image;
}

@end
