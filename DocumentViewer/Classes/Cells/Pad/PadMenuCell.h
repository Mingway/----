//
//  PadMenuCell.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PadMenuCell : UITableViewCell

- (void)setTitle:(NSString *)title;

@end
