//
//  PadMenuCell.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PadMenuCell.h"

@interface PadMenuCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation PadMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setTitle:(NSString *)title{
    self.titleLabel.text = title;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
