//
//  FTPConnectionManager.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-28.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FTPConnectionManagerDelegate <NSObject>

- (void)ftpConnectSuccessListEntries:(NSArray *)entries;
- (void)ftpConnectFailed;

@end

@interface FTPConnectionManager : NSObject

@property (nonatomic) id <FTPConnectionManagerDelegate> delegate;

+ (instancetype) shareInstance;
- (void)connectWithHostName:(NSString *)hostName username:(NSString *)username password:(NSString *)password delegate:(id <FTPConnectionManagerDelegate>) delegate;
- (void)listPath:(NSString*)path;

@end
