//
//  FTPConnectionManager.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-28.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "FTPConnectionManager.h"
#import "ACFTPClient.h"

@interface FTPConnectionManager ()<ACFTPClientDelegate>{
    ACFTPClient *_client;
}

@end

@implementation FTPConnectionManager

+ (instancetype) shareInstance{
    static dispatch_once_t onceToken;
    static FTPConnectionManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[FTPConnectionManager alloc] init];
    });
    return manager;
}

- (void)connectWithHostName:(NSString *)hostName username:(NSString *)username password:(NSString *)password delegate:(id <FTPConnectionManagerDelegate>) delegate{
    self.delegate = delegate;
    if (!([_client.location.host isEqualToString:hostName] && [_client.location.username isEqualToString:username] && [_client.location.password isEqualToString:password]) || !_client) {
        _client = [ACFTPClient clientWithHost:hostName username:username password:password];
    }
    [_client setDelegate:self];
    [_client list:@""];
}

- (void)listPath:(NSString*)path{
    [_client list:path];
}

#pragma mark -
#pragma mark ACFTPClientDelegate

-(void)client:(ACFTPClient*)client request:(id)request didListEntries:(NSArray*)entries{
    if (_delegate && [_delegate respondsToSelector:@selector(ftpConnectSuccessListEntries:)]) {
        [_delegate ftpConnectSuccessListEntries:entries];
    }
}

-(void)client:(ACFTPClient*)client request:(id)request didUpdateProgress:(float)progress{
    
}

-(void)client:(ACFTPClient*)client request:(id)request didDownloadFile:(NSURL*)sourceURL toDestination:(NSString*)destinationPath{
    
}

-(void)client:(ACFTPClient*)client request:(id)request didUploadFile:(NSString*)sourcePath toDestination:(NSURL*)destination{
    
}

-(void)client:(ACFTPClient*)client request:(id)request didMakeDirectory:(NSURL*)destination{
    
}

-(void)client:(ACFTPClient*)client request:(id)request didDeleteFile:(NSURL*)fileURL{
    
}

-(void)client:(ACFTPClient*)client request:(id)request didFailWithError:(NSError*)error{
    if (_delegate && [_delegate respondsToSelector:@selector(ftpConnectFailed)]) {
        [_delegate ftpConnectFailed];
    }
}

-(void)client:(ACFTPClient*)client request:(id)request didUpdateStatus:(NSString*)status{
    
}

-(void)client:(ACFTPClient*)client requestDidCancel:(id)request{
    
}

@end
