//
//  WebDavModelManager.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSMutableArray *webDavConnections;
@property (nonatomic, strong) NSMutableArray *ftpConnections;

+ (instancetype) defaultManager;

- (void)insertWebDavConnectionWithConnectionName:(NSString *)connectionName remoteUrl:(NSString *)remoteUrl username:(NSString *)username password:(NSString *)password;
- (void)insertFTPConnectionWithConnectionName:(NSString *)connectionName hostName:(NSString *)hostName username:(NSString *)username password:(NSString *)password;

- (void)saveContext;

@end
