//
//  WebDavModelManager.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "ModelManager.h"
#import <CoreData/CoreData.h>
#import "WebDavInfo.h"
#import "FTPInfo.h"

@implementation ModelManager{
    NSManagedObjectContext          *_managedObjectContext;
    NSManagedObjectModel            *_managedObjectModel;
    NSPersistentStoreCoordinator    *_persistentStoreCoordinator;
}

#pragma mark - Static methods

@dynamic managedObjectContext, managedObjectModel, persistentStoreCoordinator;


- (id)init
{
    self = [super init];
    if (self) {
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"WebDavInfo"];
        [request setFetchBatchSize:20];
        
        /*
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"musicId" ascending:NO];
        NSArray *sortDescriptors = @[sortDescriptor];
        [request setSortDescriptors:sortDescriptors];
        */
        
        NSError *error;
        NSArray *fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
        if (fetchResults == nil) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        self.webDavConnections = [fetchResults mutableCopy];
        request = nil;
        error = nil;
        fetchResults = nil;
        
        request = [[NSFetchRequest alloc] initWithEntityName:@"FTPInfo"];
        [request setFetchBatchSize:20];
        
        fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
        if (fetchResults == nil) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        self.ftpConnections = [fetchResults mutableCopy];
    }
    return self;
}

+ (instancetype) defaultManager
{
    static ModelManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ModelManager alloc]init];
    });
    return instance;
}

- (void)insertWebDavConnectionWithConnectionName:(NSString *)connectionName remoteUrl:(NSString *)remoteUrl username:(NSString *)username password:(NSString *)password{
    WebDavInfo *webDav = (WebDavInfo*)[NSEntityDescription insertNewObjectForEntityForName:@"WebDavInfo" inManagedObjectContext:self.managedObjectContext];
    webDav.connectionName = connectionName;
    webDav.remoteUrl = remoteUrl;
    webDav.username = username;
    webDav.password = password;
    
    [self.webDavConnections insertObject:webDav atIndex:0];
    
    [self saveContext];
}

- (void)insertFTPConnectionWithConnectionName:(NSString *)connectionName hostName:(NSString *)hostName username:(NSString *)username password:(NSString *)password{
    FTPInfo *ftp = (FTPInfo*)[NSEntityDescription insertNewObjectForEntityForName:@"FTPInfo" inManagedObjectContext:self.managedObjectContext];
    ftp.connectionName = connectionName;
    ftp.hostName = hostName;
    ftp.username = username;
    ftp.password = password;
    
    [self.ftpConnections insertObject:ftp atIndex:0];
    
    [self saveContext];
}

#pragma mark -
#pragma mark CoreData Operation


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:nil];
        }
    }
}


#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}


// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@".Connections.sqlite"];
    
    NSError *error;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    // Allow inferred migration from the original version of the application.
    NSDictionary *options = @{ NSMigratePersistentStoresAutomaticallyOption : @YES, NSInferMappingModelAutomaticallyOption : @YES };
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
