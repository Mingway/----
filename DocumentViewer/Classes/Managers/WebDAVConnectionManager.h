//
//  WebDAVConnectionManager.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WebDAVConnectionManagerDelegate <NSObject>

@required
- (void)webDavConnectSuccessWithProperties:(NSArray*)items;
- (void)webDavConnectFailed;

@end

@interface WebDAVConnectionManager : NSObject

@property (nonatomic) id <WebDAVConnectionManagerDelegate> delegate;

+ (instancetype)shareInstance;
- (void)connectWithRemoteUrl:(NSString *)remoteUrl username:(NSString *)username password:(NSString *)password delegate:(id <WebDAVConnectionManagerDelegate>) delegate;

@end
