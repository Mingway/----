//
//  WebDAVConnectionManager.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "WebDAVConnectionManager.h"
#import "ACWebDAVClient.h"

@interface WebDAVConnectionManager ()<ACWebDAVPropertyRequestDelegate,
ACWebDAVClientDelegate>{
    ACWebDAVClient *_client;
}

@end

@implementation WebDAVConnectionManager

+ (instancetype)shareInstance{
    static dispatch_once_t onceToken;
    static WebDAVConnectionManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[WebDAVConnectionManager alloc] init];
    });
    return manager;
}

- (void)connectWithRemoteUrl:(NSString *)remoteUrl username:(NSString *)username password:(NSString *)password delegate:(id <WebDAVConnectionManagerDelegate>) delegate{
    self.delegate = delegate;
    _client = [ACWebDAVClient clientWithHost:remoteUrl username:username password:password];
    [_client setDelegate:self];
    [_client loadMetadata:@""];
}

#pragma mark -
#pragma mark ACWebDAVClientDelegate

- (void)client:(ACWebDAVClient*)client failedWithError:(NSError*)error{
    NSLog(@"fail %@",error.description);
    if (_delegate && [_delegate respondsToSelector:@selector(webDavConnectFailed)]) {
        [_delegate webDavConnectFailed];
    }
}

- (void)client:(ACWebDAVClient*)client failedWithErrorCode:(int)errorCode{
    NSLog(@"error code = %d",errorCode);
    if (_delegate && [_delegate respondsToSelector:@selector(webDavConnectFailed)]) {
        [_delegate webDavConnectFailed];
    }
}

- (void)client:(ACWebDAVClient*)client loadedMetadata:(ACWebDAVItem*)item{
    NSLog(@"receive %@",item);
    if (_delegate && [_delegate respondsToSelector:@selector(webDavConnectSuccessWithProperties:)]) {
        if (item) {
            [_delegate webDavConnectSuccessWithProperties:@[item]];
        }
    }
}

- (void)client:(ACWebDAVClient*)client loadMetadataFailedWithError:(NSError*)error{
    NSLog(@"fail %@",error.description);
    if (_delegate && [_delegate respondsToSelector:@selector(webDavConnectFailed)]) {
        [_delegate webDavConnectFailed];
    }
}

- (void)client:(ACWebDAVClient*)client loadMetadataFailedWithErrorCode:(int)errorCode{
    NSLog(@"error code = %d",errorCode);
    if (_delegate && [_delegate respondsToSelector:@selector(webDavConnectFailed)]) {
        [_delegate webDavConnectFailed];
    }
}

@end
