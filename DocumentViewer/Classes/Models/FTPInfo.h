//
//  FTPInfo.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-28.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FTPInfo : NSManagedObject

@property (nonatomic, retain) NSString * connectionName;
@property (nonatomic, retain) NSString * hostName;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;

@end
