//
//  FTPInfo.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-28.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "FTPInfo.h"


@implementation FTPInfo

@dynamic connectionName;
@dynamic hostName;
@dynamic username;
@dynamic password;

@end
