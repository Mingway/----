//
//  WebDavInfo.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WebDavInfo : NSManagedObject

@property (nonatomic, retain) NSString * connectionName;
@property (nonatomic, retain) NSString * remoteUrl;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;

@end
