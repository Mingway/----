//
//  WebDavInfo.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "WebDavInfo.h"


@implementation WebDavInfo

@dynamic connectionName;
@dynamic remoteUrl;
@dynamic username;
@dynamic password;

@end
