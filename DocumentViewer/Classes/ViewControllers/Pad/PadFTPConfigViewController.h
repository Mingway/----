//
//  PadFTPConfigViewController.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-28.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FTPInfo;
@interface PadFTPConfigViewController : UITableViewController

- (void)setEditModeWithFTPInfo:(FTPInfo *)ftpInfo;

@end
