//
//  PadFTPConfigViewController.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-28.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PadFTPConfigViewController.h"
#import "MFSideMenu.h"
#import "FTPConnectionManager.h"
#import "PadFileListViewController.h"
#import "FTPInfo.h"
#import "ModelManager.h"

@interface PadFTPConfigViewController ()<UITextFieldDelegate, FTPConnectionManagerDelegate>{
    ModelManager *_modelManager;
    FTPInfo *_ftpInfo;
    BOOL _isEditMode;
}

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *hostTextField;
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
@property (weak, nonatomic) IBOutlet UITextField *portTextField;
@property (weak, nonatomic) IBOutlet UITextField *pathTextField;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
- (IBAction)connect:(UIButton *)sender;

@end

@implementation PadFTPConfigViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_ftpInfo) {
        self.titleTextField.text = _ftpInfo.connectionName;
        self.hostTextField.text = _ftpInfo.hostName;
        self.userTextField.text = _ftpInfo.username;
        self.passTextField.text = _ftpInfo.password;
        
        _isEditMode = YES;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(menu:)];
    self.navigationItem.leftBarButtonItems = @[menuBarButton];
    _modelManager = [ModelManager defaultManager];
}

- (void)setEditModeWithFTPInfo:(FTPInfo *)ftpInfo{
    _ftpInfo = ftpInfo;
}

#pragma mark -
#pragma mark - Bar Button Action

- (void)menu:(UIBarButtonItem *)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

#pragma mark -
#pragma mark Connect

- (IBAction)connect:(UIButton *)sender {
    if (self.hostTextField.text && ![self.hostTextField.text isEqualToString:@""] && self.userTextField.text && ![self.userTextField.text isEqualToString:@""] && self.passTextField.text) {
        
        [[FTPConnectionManager shareInstance] connectWithHostName:self.hostTextField.text username:self.userTextField.text password:self.passTextField.text delegate:self];
        [sender setEnabled:NO];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请填写完整信息" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark FTPConnectionDelegate

- (void)ftpConnectSuccessListEntries:(NSArray *)entries{
    NSString *connectionName = nil;
    if ([self.titleTextField.text isEqualToString:@""]) {
        connectionName = @"FTP连接账号";
    }else{
        connectionName = self.titleTextField.text;
    }
    if (!_isEditMode) {
        [_modelManager insertFTPConnectionWithConnectionName:connectionName hostName:self.hostTextField.text username:self.userTextField.text password:self.passTextField.text];
    }else{
        _ftpInfo.connectionName = self.titleTextField.text;
        _ftpInfo.hostName = self.hostTextField.text;
        _ftpInfo.username = self.userTextField.text;
        _ftpInfo.password = self.passTextField.text;
        
        [_modelManager saveContext];
    }
    
    PadFileListViewController *fileList = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadFileListViewController"];
    [fileList setType:CTFTP];
    [fileList setEntries:entries];
    [self.navigationController pushViewController:fileList animated:YES];
    [self.connectButton setEnabled:YES];
}

- (void)ftpConnectFailed{
    [self.connectButton setEnabled:YES];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"连接失败，请检查信息填写是否正确。" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
