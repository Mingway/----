//
//  PadFileListController.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    CTWebDav = 1,
    CTFTP
}ConnectionType;

@class WebDavInfo;
@class FTPInfo;

@interface PadFileListViewController : UITableViewController

@property (nonatomic, assign) ConnectionType type;
@property (nonatomic, strong) NSArray *properties;
@property (nonatomic, strong) NSArray *entries;

- (void)loadPropertiesWithWebDavInfo:(WebDavInfo *)webDavInfo;
- (void)loadEntriesWithFTPInfo:(FTPInfo *)ftpInfo;

@end
