//
//  PadFileListController.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PadFileListViewController.h"
#import "WebDavInfo.h"
#import "WebDAVConnectionManager.h"
#import "MFSideMenu.h"
#import "ACWebDAVFile.h"
#import "ACFTPEntry.h"
#import "FTPConnectionManager.h"
#import "FTPInfo.h"

@interface PadFileListViewController ()<WebDAVConnectionManagerDelegate, FTPConnectionManagerDelegate>

@end

@implementation PadFileListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - Bar Button Action

- (void)menu:(UIBarButtonItem *)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

#pragma mark -
#pragma mark Public Method

- (void)setEntries:(NSArray *)entries{
    _entries = entries;
    [self.tableView reloadData];
}

- (void)setProperties:(NSArray *)properties{
    _properties = properties;
    [self.tableView reloadData];
}

- (void)loadPropertiesWithWebDavInfo:(WebDavInfo *)webDavInfo{
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(menu:)];
    self.navigationItem.leftBarButtonItems = @[menuBarButton];
    
    self.title = webDavInfo.connectionName;
    
    [[WebDAVConnectionManager shareInstance] connectWithRemoteUrl:[NSURL URLWithString:webDavInfo.remoteUrl] username:webDavInfo.username password:webDavInfo.password delegate:self];
}

- (void)loadEntriesWithFTPInfo:(FTPInfo *)ftpInfo{
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(menu:)];
    self.navigationItem.leftBarButtonItems = @[menuBarButton];
    
    self.title = ftpInfo.connectionName;
    
    [[FTPConnectionManager shareInstance] connectWithHostName:ftpInfo.hostName username:ftpInfo.username password:ftpInfo.password delegate:self];
}

#pragma mark -
#pragma mark WebDAVConnectionManagerDelegate

- (void)webDavConnectSuccessWithProperties:(NSArray *)items{
    [self setProperties:items];
}

- (void)webDavConnectFailed{
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (self.type) {
        case CTWebDav:
            return self.properties ? self.properties.count : 0;
            break;
        case CTFTP:
            return self.entries ? self.entries.count : 0;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FileCell" forIndexPath:indexPath];
    
    switch (self.type) {
        case CTWebDav:{
            if ([self.properties[indexPath.row] isKindOfClass:[ACWebDAVFile class]]) {
                ACWebDAVFile *file = self.properties[indexPath.row];
                cell.textLabel.text = file.contentType;
                NSLog(@"%lld",file.contentLength);
            }
        }
            break;
        case CTFTP:{
            ACFTPEntry *entry = self.entries[indexPath.row];
            cell.textLabel.text = entry.name;
            if (entry.type == FTPEntryTypeDirectory) {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
        }
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (self.type) {
        case CTWebDav:{
            
        }
            break;
        case CTFTP:{
            ACFTPEntry *entry = self.entries[indexPath.row];
            if (entry.type == FTPEntryTypeDirectory) {
                [[FTPConnectionManager shareInstance] setDelegate:self];
                if ([entry.parent.href isEqualToString:@"/"]) {
                    [[FTPConnectionManager shareInstance] listPath:[NSString stringWithFormat:@"%@%@",entry.parent.href,entry.name]];
                    NSLog(@"%@",[NSString stringWithFormat:@"%@%@",entry.parent.href,entry.name]);
                }else{
                    [[FTPConnectionManager shareInstance] listPath:[NSString stringWithFormat:@"%@/%@",entry.parent.href,entry.name]];
                    NSLog(@"%@",[NSString stringWithFormat:@"%@/%@",entry.parent.href,entry.name]);
                }
            }
        }
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark FTPConnectionManagerDelegate

- (void)ftpConnectSuccessListEntries:(NSArray *)entries{
    PadFileListViewController *fileList = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadFileListViewController"];
    [fileList setType:CTFTP];
    [fileList setEntries:entries];
    [self.navigationController pushViewController:fileList animated:YES];
}

- (void)ftpConnectFailed{
    NSLog(@"ftp connect fail");
}

@end
