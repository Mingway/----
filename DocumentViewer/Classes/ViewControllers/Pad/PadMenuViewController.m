//
//  PadImportSourceListViewController.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PadMenuViewController.h"
#import "MFSideMenu.h"
#import "PadRootViewController.h"
#import "AppDelegate.h"
#import "PadWebDavConfigViewController.h"
#import "PadMenuCell.h"
#import "ModelManager.h"
#import "PadFileListViewController.h"
#import "WebDavInfo.h"
#import "PadFTPConfigViewController.h"
#import "FTPInfo.h"

@interface PadMenuViewController (){
    ModelManager *_modelManager;
}

@end

@implementation PadMenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _modelManager = [ModelManager defaultManager];
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self.tableView selector:@selector(reloadData) name:@"reloadData" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_modelManager.webDavConnections && _modelManager.ftpConnections && _modelManager.webDavConnections.count > 0 && _modelManager.ftpConnections.count > 0) {
        return 4;
    }else if((_modelManager.webDavConnections && _modelManager.webDavConnections.count > 0)|| (_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0)){
        return 3;
    }else{
        return 2;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            return 1;
        }
            break;
        case 1:{
            return 3;
        }
            break;
        case 2:{
            if (_modelManager.webDavConnections && _modelManager.webDavConnections.count > 0) {
                return _modelManager.webDavConnections ? _modelManager.webDavConnections.count : 0;
            }else if(_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
                return _modelManager.ftpConnections ? _modelManager.ftpConnections.count : 0;
            }else{
                return 0;
            }
        }
            break;
        case 3:{
            if(_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
                return _modelManager.ftpConnections ? _modelManager.ftpConnections.count : 0;
            }else{
                return 0;
            }
        }
            break;
        default:{
            return 0;
        }
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PadMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell" forIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0:{
            [cell setTitle:@"文档列表"];
        }
            break;
        case 1:{
            switch (indexPath.row) {
                case 0:{
                    [cell setTitle:@"WebDAV"];
                }
                    break;
                case 1:{
                    [cell setTitle:@"DropBox"];
                }
                    break;
                case 2:{
                    [cell setTitle:@"FTP"];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:{
            if (_modelManager.webDavConnections && _modelManager.webDavConnections.count > 0) {
                WebDavInfo *webDavInfo = _modelManager.webDavConnections[indexPath.row];
                [cell setTitle:webDavInfo.connectionName];
            }else if (_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
                FTPInfo *ftpInfo = _modelManager.ftpConnections[indexPath.row];
                [cell setTitle:ftpInfo.connectionName];
            }
            
            [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
        }
        case 3:{
            if (_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
                FTPInfo *ftpInfo = _modelManager.ftpConnections[indexPath.row];
                [cell setTitle:ftpInfo.connectionName];
            }
            
            [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            return @"主页";
        }
            break;
        case 1:{
            return @"选择文档来源";
        }
            break;
        case 2:{
            if (_modelManager.webDavConnections && _modelManager.webDavConnections.count > 0) {
                return @"WebDav账号";
            }else if(_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
                return @"FTP账号";
            }else{
                return nil;
            }
        }
        case 3:{
            if(_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
                return @"FTP账号";
            }else{
                return nil;
            }
        }
            break;
        default:
            return nil;
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        //主页文档列表
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:delegate.padRootViewController];
        navigationController.viewControllers = controllers;
    }else if(indexPath.section == 1){
        //选择文档来源
        switch (indexPath.row) {
            case 0:{
                PadWebDavConfigViewController *webDavController = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadWebDavConfigViewController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:webDavController];
                navigationController.viewControllers = controllers;
            }
                break;
            case 1:{
                
            }
                break;
            case 2:{
                PadFTPConfigViewController *ftpController = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadFTPConfigViewController"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:ftpController];
                navigationController.viewControllers = controllers;
            }
                break;
            default:
                break;
        }
    }else if(indexPath.section == 2){
        if (_modelManager.webDavConnections  && _modelManager.webDavConnections.count > 0) {
            //WebDAV账号
            [self connectWebDavWithIndexPath:indexPath];
        }else if (_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
            //FTP账号
            [self connectFTPWithIndexPath:indexPath];
        }
    }else if (indexPath.section == 3){
        if (_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
            //FTP账号
            [self connectFTPWithIndexPath:indexPath];
        }
    }
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark Connect

- (void)connectWebDavWithIndexPath:(NSIndexPath *)indexPath{
    PadFileListViewController *fileListController = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadFileListViewController"];
    [fileListController loadPropertiesWithWebDavInfo:_modelManager.webDavConnections[indexPath.row]];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:fileListController];
    navigationController.viewControllers = controllers;
}

- (void)connectFTPWithIndexPath:(NSIndexPath *)indexPath{
    PadFileListViewController *fileListController = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadFileListViewController"];
    [fileListController loadEntriesWithFTPInfo:_modelManager.ftpConnections[indexPath.row]];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:fileListController];
    navigationController.viewControllers = controllers;
}

#pragma mark -
#pragma mark accessoryButton action

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 2){
        if (_modelManager.webDavConnections  && _modelManager.webDavConnections.count > 0) {
            //WebDAV账号
            [self configWebDavWithIndexPath:indexPath];
        }else if (_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
            //FTP账号
            [self configFTPWithIndexPath:indexPath];
        }
    }else if (indexPath.section == 3){
        if (_modelManager.ftpConnections && _modelManager.ftpConnections.count > 0){
            //FTP账号
            [self configFTPWithIndexPath:indexPath];
        }
    }
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

#pragma mark -
#pragma mark Config

- (void)configWebDavWithIndexPath:(NSIndexPath *)indexPath{
    PadWebDavConfigViewController *webDavController = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadWebDavConfigViewController"];
    [webDavController setEditModeWithWebDavInfo:_modelManager.webDavConnections[indexPath.row]];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:webDavController];
    navigationController.viewControllers = controllers;
}

- (void)configFTPWithIndexPath:(NSIndexPath *)indexPath{
    PadFTPConfigViewController *ftpController = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadFTPConfigViewController"];
    [ftpController setEditModeWithFTPInfo:_modelManager.ftpConnections[indexPath.row]];
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:ftpController];
    navigationController.viewControllers = controllers;
}

@end
