//
//  PadRootViewController.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-16.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PadRootViewController.h"
#import "PadDocumentCell.h"
#import "DirectoryWatcher.h"
#import "PadWebDavConfigViewController.h"
#import "MFSideMenu.h"

@import QuickLook;
@import MessageUI;

@interface PadRootViewController ()<UICollectionViewDelegate,
                                    UICollectionViewDataSource,
                                    DirectoryWatcherDelegate,
                                    UIDocumentInteractionControllerDelegate,
                                    QLPreviewControllerDataSource,
                                    QLPreviewControllerDelegate,
                                    MFMailComposeViewControllerDelegate>{
    DirectoryWatcher *_docWatcher;
    NSMutableArray *_documentURLs;
    UIDocumentInteractionController *_docInteractionController;
}

@end

@implementation PadRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(menu:)];
    self.navigationItem.leftBarButtonItems = @[menuBarButton];
    
    UIBarButtonItem *adviceBarButton = [[UIBarButtonItem alloc] initWithTitle:@"意见反馈" style:UIBarButtonItemStylePlain target:self action:@selector(emailHandler:)];
    self.navigationItem.rightBarButtonItems = @[adviceBarButton];
    
    _documentURLs = [NSMutableArray array];
    [self addDirectoryWatcher];
}

- (void)addDirectoryWatcher{
    _docWatcher = [DirectoryWatcher watchFolderWithPath:[self applicationDocumentsDirectory] delegate:self];
    
    [self directoryDidChange:_docWatcher];
}

- (void)setupDocumentControllerWithURL:(NSURL *)url{
    if (!_docInteractionController ){
        _docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        _docInteractionController.delegate = self;
    }else{
        _docInteractionController.URL = url;
    }
}

#pragma mark -
#pragma mark Open Documents From InBox

- (void)handleOpenDocumentFromInbox:(NSURL *)fileURL{
    BOOL isSuccess = [self copyMissingFile:fileURL.path toPath:[self applicationDocumentsDirectory]];
    if (isSuccess) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"导入成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }else{
        NSLog(@"copy file failed");
    }
}

- (BOOL)copyMissingFile:(NSString *)sourcePath toPath:(NSString *)toPath{
    BOOL retVal = YES;
    NSString * finalLocation = [toPath stringByAppendingPathComponent:[sourcePath lastPathComponent]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:finalLocation]){
        NSError *error;
        retVal = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:finalLocation error:&error];
        if (error) {
            NSLog(@"%@",error.description);
        }
    }
    return retVal;
}

#pragma mark -
#pragma mark - Bar Button Action

- (void)menu:(UIBarButtonItem *)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (void)emailHandler:(UIBarButtonItem *)sender{
    if ([self canSendMail]) {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        [mailController setMailComposeDelegate:self];
        [mailController setToRecipients:@[@"mingway1991@gmail.com"]];
        [mailController setSubject:@"文档预览意见反馈"];
        [self presentViewController:mailController animated:YES completion:nil];
    }
}

-(BOOL)canSendMail{
	BOOL isResult = NO;
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil){
        if ([mailClass canSendMail]){
			isResult = YES;
        }
    }
	return isResult;
}

#pragma mark -
#pragma mark EmailDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    switch (result){
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - DirectoryWatcher Delegate

- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher{
    [_documentURLs removeAllObjects];
	
	NSString *documentsDirectoryPath = [self applicationDocumentsDirectory];
	
	NSArray *documentsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectoryPath
                                                                                              error:NULL];
    
	for (NSString* curFileName in [documentsDirectoryContents objectEnumerator])
	{
		NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:curFileName];
		NSURL *fileURL = [NSURL fileURLWithPath:filePath];
		
		BOOL isDirectory;
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        if (!(isDirectory && [curFileName isEqualToString:@"Inbox"]) && ![[curFileName pathExtension] isEqualToString:@"sqlite"] && ![[curFileName pathExtension] isEqualToString:@"sqlite-shm"] && ![[curFileName pathExtension] isEqualToString:@"sqlite-wal"]){
            [_documentURLs addObject:fileURL];
        }
	}
	
	[self.collectionView reloadData];
}

#pragma mark -
#pragma mark - File system support

- (NSString *)applicationDocumentsDirectory{
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark -
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
}

#pragma mark -
#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _documentURLs ? _documentURLs.count : 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PadDocumentCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DocumentCell" forIndexPath:indexPath];
    
    NSURL *fileURL = _documentURLs[indexPath.item];
    [self setupDocumentControllerWithURL:fileURL];
    [cell setTitle:[[fileURL path] lastPathComponent]];
    
    NSString *fileURLString = [_docInteractionController.URL path];
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:fileURLString error:nil];
    NSInteger fileSize = [[fileAttributes objectForKey:NSFileSize] intValue];
    NSString *fileSizeStr = [NSByteCountFormatter stringFromByteCount:fileSize
                                                           countStyle:NSByteCountFormatterCountStyleFile];
    [cell setDetail:[NSString stringWithFormat:@"%@ - %@", fileSizeStr, _docInteractionController.UTI]];
    
    NSInteger iconCount = [_docInteractionController.icons count];
    [cell setPreviewImage:[_docInteractionController.icons objectAtIndex:iconCount - 1]];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    QLPreviewController *previewController = [[QLPreviewController alloc] init];
    previewController.dataSource = self;
    previewController.delegate = self;
    
    previewController.currentPreviewItemIndex = indexPath.item;
    [previewController setEditing:YES animated:YES];
    [[self navigationController] pushViewController:previewController animated:YES];
}

#pragma mark - UIDocumentInteractionControllerDelegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController{
    return self;
}


#pragma mark - QLPreviewControllerDataSource

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController{
    NSInteger numToPreview = 0;
    
    numToPreview = _documentURLs.count;
    
    return numToPreview;
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller{
    
}

- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx{
    NSURL *fileURL = nil;
    
    fileURL = [_documentURLs objectAtIndex:idx];
    
    return fileURL;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
