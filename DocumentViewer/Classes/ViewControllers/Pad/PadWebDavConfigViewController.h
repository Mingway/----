//
//  PadWebDavConfigViewController.h
//  DocumentViewer
//
//  Created by shimingwei on 14-7-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WebDavInfo;
@interface PadWebDavConfigViewController : UITableViewController

- (void)setEditModeWithWebDavInfo:(WebDavInfo *)webDavInfo;

@end
