//
//  PadWebDavConfigViewController.m
//  DocumentViewer
//
//  Created by shimingwei on 14-7-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PadWebDavConfigViewController.h"
#import "MFSideMenu.h"
#import "WebDAVConnectionManager.h"
#import "PadFileListViewController.h"
#import "ModelManager.h"
#import "WebDavInfo.h"

@interface PadWebDavConfigViewController ()<UITextFieldDelegate,
                                            WebDAVConnectionManagerDelegate>{
    ModelManager *_modelManager;
    WebDavInfo *_webDavInfo;
    BOOL _isEditMode;
}

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITextField *urlTextField;
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;

- (IBAction)connect:(UIButton *)sender;


@end

@implementation PadWebDavConfigViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_webDavInfo) {
        self.titleTextField.text = _webDavInfo.connectionName;
        self.urlTextField.text = _webDavInfo.remoteUrl;
        self.userTextField.text = _webDavInfo.username;
        self.passTextField.text = _webDavInfo.password;
        
        _isEditMode = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(menu:)];
    self.navigationItem.leftBarButtonItems = @[menuBarButton];
    _modelManager = [ModelManager defaultManager];
}

- (void)setEditModeWithWebDavInfo:(WebDavInfo *)webDavInfo{
    _webDavInfo = webDavInfo;
}

#pragma mark -
#pragma mark - Bar Button Action

- (void)menu:(UIBarButtonItem *)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

#pragma mark -
#pragma mark Connect

- (IBAction)connect:(UIButton *)sender {
    if (self.urlTextField.text && ![self.urlTextField.text isEqualToString:@""] && self.userTextField.text && ![self.userTextField.text isEqualToString:@""] && self.passTextField.text) {
        [[WebDAVConnectionManager shareInstance] connectWithRemoteUrl:self.urlTextField.text username:self.userTextField.text password:self.passTextField.text delegate:self];
        [sender setEnabled:NO];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"请填写完整信息" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

#pragma mark -
#pragma mark WebDAVConnectionManagerDelegate

- (void)webDavConnectSuccessWithProperties:(NSArray *)items{
    NSString *connectionName = nil;
    if ([self.titleTextField.text isEqualToString:@""]) {
        connectionName = @"WebDAV连接账号";
    }else{
        connectionName = self.titleTextField.text;
    }
    if (!_isEditMode) {
        [_modelManager insertWebDavConnectionWithConnectionName:connectionName remoteUrl:self.urlTextField.text username:self.userTextField.text password:self.passTextField.text];
    }else{
        _webDavInfo.connectionName = self.titleTextField.text;
        _webDavInfo.remoteUrl = self.urlTextField.text;
        _webDavInfo.username = self.userTextField.text;
        _webDavInfo.password = self.passTextField.text;
        
        [_modelManager saveContext];
    }
    
    PadFileListViewController *fileList = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil] instantiateViewControllerWithIdentifier:@"PadFileListViewController"];
    [fileList setType:CTWebDav];
    [fileList setProperties:items];
    [self.navigationController pushViewController:fileList animated:YES];
    [self.connectButton setEnabled:YES];
}

- (void)webDavConnectFailed{
    [self.connectButton setEnabled:YES];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"连接失败，请检查信息填写是否正确。" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
